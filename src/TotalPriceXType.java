

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.Collections;

public class TotalPriceXType {
    //attributi
    private ArrayList <Prezzo> prezzi = new ArrayList<>();
    //costruttore
    public TotalPriceXType()
    {
        prezzi = new ArrayList<Prezzo>();
    }
   public void CalcolaPrezzo(ArrayList <Edifici> e)
   {
        int sommaCondo=0,sommaMulty=0,sommaRes=0,sommaUnk=0;
        for(Edifici s: e)
        {
            switch(s.getType())
            {
                case "Condo":sommaCondo+=s.getPrice();break;
                case "Multi-Family":sommaMulty+=s.getPrice();break;
                case "Residential":sommaRes+=s.getPrice();break;
                case "Unkown":sommaUnk+=s.getPrice();break;
            }
        }
        BigDecimal somma1 = new BigDecimal(sommaCondo);
        BigDecimal somma2 = new BigDecimal(sommaMulty);
        BigDecimal somma3 = new BigDecimal(sommaRes);
        BigDecimal somma4 = new BigDecimal(sommaUnk);
        prezzi.add(new Prezzo("Condo", somma1));
        prezzi.add(new Prezzo("MultiFamily", somma2));
        prezzi.add(new Prezzo("Residential", somma3));
        prezzi.add(new Prezzo("Unkown", somma4));
   }
   public void sort()
   {
       Collections.sort(prezzi);
   }
    
    @Override
    public String toString()
    {
        String s="";
        for(Prezzo p: prezzi)
            {
                s+=p.toString();
            }
        return s;
    }


    
}
