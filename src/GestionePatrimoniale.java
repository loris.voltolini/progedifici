
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

public class GestionePatrimoniale {
    // attributi
    private ArrayList<Edifici> listaP = new ArrayList<Edifici>();

    // costruttore
    public GestionePatrimoniale() {
        listaP = new ArrayList<Edifici>();
    }

    // inserire nuovi edifici;
    public void InserisciEdificio(Edifici e) {
        listaP.add(e);
    }

    // 2) eliminare un edificio presente;
    public void eliminaEdificio(Edifici e) {
        listaP.remove(e);
    }
  

    // 4) ordinare in modo crescente gli edifici in base al prezzo;
    public void sort() 
    {
        Collections.sort(listaP);
    }
  // 5) salvare/caricare in formato
    //binario(ObjectOutputStream/ObjectInputStream) 
    public void salva() throws java.io.IOException 
    {
        final FileOutputStream f = new FileOutputStream(new File("lista.bin"));
        ObjectOutputStream o = new ObjectOutputStream(f);
        
        o.writeObject(this.listaP);
        f.close();
                
        
    }


    @SuppressWarnings("unchecked") // serve per dare un segnale d'errore perchè non è sicuro che il cast(74)
    // restitutisca qualcosa
    public void carica() throws java.io.IOException 
        {

            FileInputStream fi = new FileInputStream(new File("lista.bin"));
            ObjectInputStream oi = new ObjectInputStream(fi);
            try 
                { 
                this.listaP = (ArrayList<Edifici>) oi.readObject();
                } 
            catch (ClassNotFoundException exception) 
                {
                    oi.close();
                }

        }
    public void loadCSV() throws IOException
        {
            Scanner scanner = new Scanner(new File("Sacramentorealestatetransactions.csv")).useDelimiter(System.lineSeparator());
            try
                {
                    while (scanner.hasNext()) 
                    {
                        final String[] valori = scanner.next().split(";");
                        switch(valori[7])
                        {   case "Condo":InserisciEdificio(new Condo(valori[0], valori[1], valori[3], valori[7], valori[8],Integer.valueOf(valori[2]),  Integer.valueOf(valori[4]), Integer.valueOf(valori[5]),Integer.valueOf(valori[6]),Double.valueOf(valori[9]),Double.valueOf(valori[10]), Double.valueOf(valori[11])));
                                        break;
                            case "Multi-Family":InserisciEdificio(new MultiFamily(valori[0], valori[1], valori[3], valori[7], valori[8],Integer.valueOf(valori[2]),  Integer.valueOf(valori[4]), Integer.valueOf(valori[5]),Integer.valueOf(valori[6]),Double.valueOf(valori[9]),Double.valueOf(valori[10]), Double.valueOf(valori[11])));
                                                break;
                            case "Residential":InserisciEdificio(new Residential(valori[0], valori[1], valori[3], valori[7], valori[8],Integer.valueOf(valori[2]),  Integer.valueOf(valori[4]), Integer.valueOf(valori[5]),Integer.valueOf(valori[6]),Double.valueOf(valori[9]),Double.valueOf(valori[10]), Double.valueOf(valori[11])));
                                                break;
                            case "Unkown":InserisciEdificio(new Unkown(valori[0], valori[1], valori[3], valori[7], valori[8],Integer.valueOf(valori[2]),  Integer.valueOf(valori[4]), Integer.valueOf(valori[5]),Integer.valueOf(valori[6]),Double.valueOf(valori[9]),Double.valueOf(valori[10]), Double.valueOf(valori[11])));
                                            break;
                        }
                        
                    }

                 }
            catch (Exception e) {} 
            // serve per chiudere lo scanner in qualsiasi caso(errore o non)
            finally 
                {
                scanner.close();
                }
        }
//6) creare un metodo totalePrice() che restituisca 
   //per ogni tipo di edificio(type) il valore  della
    //sommatoria del campo price. */
    public String totalePrice()
    {
        double sommaCondo=0,sommaMulty=0,sommaRes=0,sommaUnk=0;
        for(Edifici e: listaP)
        {
            switch(e.getType())
            {
                case "Condo":sommaCondo+=e.getPrice();break;
                case "Multi-Family":sommaMulty+=e.getPrice();break;
                case "Residential":sommaRes+=e.getPrice();break;
                case "Unkown":sommaUnk+=e.getPrice();break;
             }
        }
       String s="";
        s="Totale prezzo Condo:" + sommaCondo+ System.lineSeparator() +
        "Totale prezzo MultiFamily:" + sommaMulty+ System.lineSeparator() +
        "Totale prezzo Residential:" + sommaRes+ System.lineSeparator() +
        "Totale prezzo Unkown:" + sommaUnk+ System.lineSeparator();
        return s;
    }
    public String totale()
    {
        TotalPriceXType t = new TotalPriceXType();
        t.CalcolaPrezzo(listaP);
        t.sort();
        return t.toString();
        //System.out.println(prova.toString());
    }
    
    
    
    // 3) stampare l'elenco degli edifici;

      @Override
      public String toString() {
          String s = "";
          for (Edifici e : listaP) {
              s += e.toString();
          }
          return s;
  
      }
}
