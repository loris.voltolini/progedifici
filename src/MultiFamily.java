import java.math.BigDecimal;

public class MultiFamily extends Edifici {
     /**
    *
    */
    private static final long serialVersionUID = 1L;

    // costruttore
     public MultiFamily(){}
     public MultiFamily(String street, String city, String state, String type,String sale_date,int zip, int beds,int baths,int sq__ft, double price,double latitude, double longitude)
         {
             super(street, city, state, type, sale_date, zip, beds, baths, sq__ft, price, latitude, longitude);
         }
     
     //metodo clone
     @Override
     public Edifici clone()
     {
         return new MultiFamily(getStreet(), getCity(), getState(), getType(), getSale_date(), getZip(), getBeds(),getBaths(), getSq__ft(), getPrice(), getLatitude(), getLongitude());
     }
     @Override
     public BigDecimal costoAffitto()
        {
            int i =200+35*getSq__ft() + 10 * getBaths();
            BigDecimal costoAffitto= new BigDecimal(i);
            return  costoAffitto;
        }
     
        @Override
        public String toString() {
           return "{" +
           " street='" + getStreet() + "'" + 
           ", city='" + getCity() + "'" + 
           ", state='" + getState() + "'" + 
           ", type='" + getType() + "'" + 
           ", sale_date='" + getSale_date() + "'"  + 
           ", zip='" + getZip() + "'" +  
           ", beds='" + getBeds() + "'" +
           ", baths='" + getBaths() + "'" +  
           ", sq__ft='" + getSq__ft() + "'"  + 
           ", price='" + getPrice() + "'"  + 
           ", latitude='" + getLatitude() + "'"  +
           ", longitude='" + getLongitude() + "'" +
           ", costoAffitto='" + costoAffitto() + "'" + System.lineSeparator();
        }
    
}
