
import java.math.BigDecimal;
public class Prezzo implements Comparable<Prezzo> {
    
    private String tipoEdificio;
    private BigDecimal price;

    public Prezzo(){}
    public Prezzo(String tipoEdificio, BigDecimal price)
        {
            this.tipoEdificio=tipoEdificio;
            this.price=price;

        }
    public Prezzo(Prezzo p)
        {
            this.tipoEdificio=p.getTipoEdificio();
            this.price=p.getPricee();
        }

    public String getTipoEdificio() { return this.tipoEdificio;}
    public void setTipoEdificio(String tipoEdificio) { this.tipoEdificio = tipoEdificio;}
    public BigDecimal getPricee() {return this.price;}
    public void setPrice(BigDecimal price) {this.price = price;}
    @Override
    public Prezzo clone()
        {
            return new Prezzo(this.getTipoEdificio(), this.getPricee());
        }
    //metodo equals
    @Override
    public boolean equals(Object obj)
        {
            if(obj instanceof Edifici)
                {
                     return equals((Edifici) obj);
                }
            else
                {
                    return false;
                }
        }
 
    public boolean equals(Prezzo e)
        {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
            return this.tipoEdificio.equals(e.getTipoEdificio()) && this.price==e.getPricee();
        }
    
    @Override
    public int compareTo(Prezzo p) 
        {
            return this.getPricee().compareTo(p.getPricee());
        }


    @Override
    public String toString() 
        {
            return " tipoEdificio='" + getTipoEdificio() + "'" + ", price='" + getPricee() + "'" + System.lineSeparator();
        }

     
    

    
}
