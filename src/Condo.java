import java.math.BigDecimal;

public class Condo extends Edifici {
    //costruttore
   
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public Condo(String street, String city, String state, String type, String sale_date, int zip, int beds, int baths,
            int sq__ft, double price, double latitude, double longitude)
        {
            super(street, city, state, type, sale_date, zip, beds, baths, sq__ft, price, latitude, longitude);
        }
    
    //metodo clone
    @Override
    public Edifici clone()
    {
        return new Condo(getStreet(), getCity(), getState(), getType(), getSale_date(), getZip(), getBeds(),getBaths(), getSq__ft(), getPrice(), getLatitude(), getLongitude());
    }
    @Override
    public BigDecimal costoAffitto()
        {
        int i =100 + 20*getSq__ft();
        BigDecimal costoAffitto= new BigDecimal(i);
            return  costoAffitto;
        }
    
    //metodo ToString
    @Override
    public String toString() {
       return "{" +
       " street='" + getStreet() + "'" + 
       ", city='" + getCity() + "'" + 
       ", state='" + getState() + "'" + 
       ", type='" + getType() + "'" + 
       ", sale_date='" + getSale_date() + "'"  + 
       ", zip='" + getZip() + "'" +  
       ", beds='" + getBeds() + "'" +
       ", baths='" + getBaths() + "'" +  
       ", sq__ft='" + getSq__ft() + "'"  + 
       ", price='" + getPrice() + "'"  + 
       ", latitude='" + getLatitude() + "'"  +
       ", longitude='" + getLongitude() + "'" +
       ", costoAffitto='" + costoAffitto() + "'" +System.lineSeparator();
}


       

    
}
