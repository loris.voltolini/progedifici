
import java.io.*;
import java.math.BigDecimal;


public abstract class Edifici implements Serializable, Comparable<Edifici>{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    // attributi
    private String street,city,state,type,sale_date;
    private int zip,beds,baths,sq__ft;
    private double price,latitude,longitude;
    
    //costruttori
    public Edifici(){}
    public Edifici(String street, String city, String state, String type,String sale_date,int zip, int beds,int baths,int sq__ft, double price,double latitude, double longitude )
        {
            this.street=street;
            this.city=city;
            this.state=state;
            this.type=type;
            this.sale_date=sale_date;
            this.zip=zip;
            this.beds=beds;
            this.baths=baths;
            this.sq__ft=sq__ft;
            this.price=price;
            this.latitude=latitude;
            this.longitude=longitude;
        }
    public Edifici(Edifici e)
        {
            this.street=e.getStreet();
            this.city=e.getCity();
            this.state=e.getState();
            this.type=e.getType();
            this.sale_date=e.getSale_date();
            this.zip=e.getZip();
            this.beds=e.getBeds();
            this.baths=e.getBaths();
            this.sq__ft=e.getSq__ft();
            this.price=e.getPrice();
            this.latitude=e.getLatitude();
            this.longitude=e.getLongitude();
        }


    //metodi get e setter
    public String getStreet() {return this.street;}
    public void setStreet(String street) {this.street = street;}
    public String getCity() {   return this.city;}
    public void setCity(String city) {   this.city = city;}
    public String getState() {   return this.state;}
    public void setState(String state){ this.state = state;}
    public String getType() {   return this.type;}
    public void setType(String type) {    this.type = type;}
    public String getSale_date() {    return this.sale_date;}
    public void setSale_date(String sale_date) {   this.sale_date = sale_date; }
    public int getZip() {    return this.zip;}
    public void setZip(int zip) { this.zip = zip;}
    public int getBeds() {   return this.beds;}
    public void setBeds(int beds) {   this.beds = beds;}
    public int getBaths() {   return this.baths;}
    public void setBaths(int baths) {  this.baths = baths;}
    public int getSq__ft() {   return this.sq__ft;}
    public void setSq__ft(int sq__ft) {    this.sq__ft = sq__ft;}
    public double getPrice() {    return this.price;}
    public void setPrice(double price) {   this.price = price;}
    public double getLatitude() {   return this.latitude;}
    public void setLatitude(double latitude) {    this.latitude = latitude;}
    public double getLongitude() {    return this.longitude;}
    public void setLongitude(double longitude) { this.longitude = longitude;}

    //metodi abstract
    public abstract Edifici clone();
    public abstract BigDecimal costoAffitto();

    //metodo equals
    @Override
    public boolean equals(Object obj)
        {
            if(obj instanceof Edifici)
                {
                    return equals((Edifici) obj);
                }
            else
                {
                    return false;
                }
        }
    public boolean equals(Edifici e)
    {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return this.street.equals(e.getStreet()) && this.city.equals(e.getCity()) &&this.state.equals(e.getState()) && this.type.equals(e.getType()) && this.sale_date.equals(e.getSale_date()) && this.zip==e.getZip() && this.beds==e.getBeds() && this.baths==e.getBaths() && this.sq__ft==e.getSq__ft() &&this.price==e.getPrice() &&this.latitude==e.getLatitude() && this.longitude==e.getLongitude();
    }
    //metodo comper to
    
    public int compareTo(Edifici e)
    {  
         int result=0;
            if(e.getPrice()<this.getPrice())
            {
                result=-1;
            }
            else
            {
                if(e.getPrice()==this.getPrice())
                {
                    result=0;
                }
                else
                {
                    if(e.getPrice()>this.getPrice())
                    {
                        result=1;
                    }
                } 
            }
            return result;
            
    }

    //metodo ToString

    @Override
    public String toString() {
        return "{" +
            " street='" + getStreet() + "'" + System.lineSeparator() +
            ", city='" + getCity() + "'" + System.lineSeparator() + 
            ", state='" + getState() + "'" + System.lineSeparator() + 
            ", type='" + getType() + "'" + System.lineSeparator() + 
            ", sale_date='" + getSale_date() + "'"  + System.lineSeparator() +
            ", zip='" + getZip() + "'" + System.lineSeparator() + 
            ", beds='" + getBeds() + "'" + System.lineSeparator() + 
            ", baths='" + getBaths() + "'" + System.lineSeparator() + 
            ", sq__ft='" + getSq__ft() + "'"  + System.lineSeparator() +
            ", price='" + getPrice() + "'"  + System.lineSeparator() +
            ", latitude='" + getLatitude() + "'"  + System.lineSeparator() +
            ", longitude='" + getLongitude() + "'" + System.lineSeparator() +
            "}";
    }

}
